# UnityWebGLTextInput
Unity WebGLはスマートフォンではテキストボックスが動作しない問題に対応するため、inputエレメントで対応する実装サンプル。
`message...`と表示された部分に文章を打った後に`Import Text`ボタンを押すと、Unity WebGL内のテキストに表示される。

# 動作確認
https://rabitbox.gitlab.io/unitywebgltextinput/

# Environment
- Unity 2020.3.25f1
