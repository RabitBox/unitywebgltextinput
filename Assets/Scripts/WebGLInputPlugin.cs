using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class UnityCanvas
{
    public int x;
    public int y;
    public int width;
    public int height;
}

public class WebGLInputPlugin
{
#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern string GetUnityCanvasJS();

    [DllImport("__Internal")]
    private static extern void CreateInputTextJS(string id, string offsetX, string offsetY, string width, string height, string placeholder);

    [DllImport("__Internal")]
    private static extern void CreateInputTextAndStyleJS(string id, string style, string placeholder);

    [DllImport("__Internal")]
    private static extern void RemoveElementJS(string id);

    [DllImport("__Internal")]
    private static extern string GetInputTextJS(string id);

    [DllImport("__Internal")]
    private static extern void ClearInputTextJS(string id);

    [DllImport("__Internal")]
    private static extern void BlurElementJS(string id);
#endif

    public static UnityCanvas GetUnityCanvas()
	{
#if UNITY_WEBGL && !UNITY_EDITOR
        var canvas = GetUnityCanvasJS();
        return JsonUtility.FromJson<UnityCanvas>( canvas );
#else
        return null;
#endif
    }

    public static void CreateInputText(string id, string offsetX, string offsetY, string width, string height, string placeholder = "")
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        CreateInputTextJS(id, offsetX, offsetY, width, height, placeholder);
#endif
    }

    public static void CreateInputTextAndStyle(string id, string style, string placeholder = "")
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        CreateInputTextAndStyleJS(id, style, placeholder);
#endif
    }

    public static string GetInputText(string id)
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        var data = GetInputTextJS(id);
        return data;//GetInputFormTextJS();
#else
        return "";
#endif
    }

    public static void ClearInputText(string id)
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        ClearInputTextJS(id);
#endif
    }

    public static void BlurElement(string id)
	{
#if UNITY_WEBGL && !UNITY_EDITOR
        BlurElementJS(id);
#endif
    }
}
