using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebGLInputer : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    private string _id = "message";

    // Start is called before the first frame update
    void Start()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = false;
#endif

        WebGLInput.captureAllKeyboardInput = false;
        var canvas = WebGLInputPlugin.GetUnityCanvas();
        if (canvas != null)
        {
            var heightSize = (int)(canvas.height / 6.0f);
            var fontSize = (int)(heightSize / 2.0f);

            var style = $"position: absolute; left: 0px; top: 0px; height: {heightSize}px; width: {canvas.width - 4}px; font-size: {fontSize}px";

            WebGLInputPlugin.CreateInputTextAndStyle(_id, style, "Messages...");
        }
    }

    public void OnPress()
    {
        var text = WebGLInputPlugin.GetInputText(_id);

        if (text != null)
        {
            _text.text = text;
            WebGLInputPlugin.ClearInputText(_id);
            WebGLInputPlugin.BlurElement(_id);
        }
    }

    public void OnPressCanvas()
    {
        WebGLInputPlugin.BlurElement(_id);
    }
}
