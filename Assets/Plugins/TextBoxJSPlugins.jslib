mergeInto(LibraryManager.library, {
  // Get unity-canvas element Layout and Size
  GetUnityCanvasJS:function(){
    const element = document.getElementById("unity-canvas");
    const clientRect = element.getBoundingClientRect();
    const offsetX = clientRect.left;
    const offsetY = clientRect.top;
    const width = element.clientWidth;
    const height = element.clientHeight;

    const json = JSON.stringify({x: offsetX, y: offsetY, width: width, height: height});
    var bufferSize = lengthBytesUTF8(json) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(json, buffer, bufferSize);

    return buffer;
  },

  // Create InputField
  CreateInputTextJS:function(id, offsetX, offsetY, width, height, placeholder){
    // Create Element
    var input_element = document.createElement('input');
    input_element.type = 'text';
    input_element.id = Pointer_stringify(id);
    input_element.placeholder = Pointer_stringify(placeholder);

    // Set Style
    input_element.style.position = "absolute";
    input_element.style.left = Pointer_stringify(offsetX) + "px";
    input_element.style.top = Pointer_stringify(offsetY) + "px";
    input_element.style.width = Pointer_stringify(width) + "px";
    input_element.style.height = Pointer_stringify(height) + "px";
    input_element.style.fontSize = "30px";
    
    // Add Child
    const parent = document.getElementById("unity-canvas");
    parent.parentElement.appendChild(input_element);
  },

  // Create InputField
  // style cssText
  CreateInputTextAndStyleJS:function(id, style, placeholder){
    // Create Element
    var input_element = document.createElement('input');
    input_element.type = 'text';
    input_element.id = Pointer_stringify(id);
    input_element.placeholder = Pointer_stringify(placeholder);

    // Set Style
    input_element.style.cssText = Pointer_stringify(style);

    // Add Child
    const parent = document.getElementById("unity-canvas");
    parent.parentElement.appendChild(input_element);
  },

  // Removes the element from a specified id
  RemoveElementJS:function(id){
    const element = document.getElementById(Pointer_stringify(id));

    if( element != null ){
      element.remove();
    }
  },

  // Get a value from a specified element(Input Element Only)
  GetInputTextJS:function(id){
    const element = document.getElementById(Pointer_stringify(id));

    if( element != null ){
      var bufferSize = lengthBytesUTF8(element.value) + 1;
      var buffer = _malloc(bufferSize);
      stringToUTF8(element.value, buffer, bufferSize);

      return buffer;
    }
    return null;
  },

  // Deletes the value of the specified element
  ClearInputTextJS:function(id){
    document.getElementById(Pointer_stringify(id)).value = "";
  },

  // Removes focus from a specified element
  BlurElementJS:function(id){
    const element = document.getElementById(Pointer_stringify(id));
    element.blur();
  },
});